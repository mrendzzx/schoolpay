<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['default_controller'] = 'Auth';
$route['translate_uri_dashes'] = FALSE;

$route['404_override'] = 'Auth/notfound';
$route['unauthorized'] = 'Auth/unauthorized';

$route['login'] = 'Auth/index';
$route['logout'] = 'Auth/logout';



// SISWA
	$route['Dashboard'] 	= 'Siswa/Dashboard';
	$route['Pembayaran'] 	= 'Siswa/Keuangan/Pembayaran';
	$route['Siswa'] 		= 'Siswa/Siswa/Siswa';
// SISWA

// Payment 
	$route['Payment/complete'] 		= 'Midtrans/complete';
	$route['Payment/pending'] 		= 'Midtrans/pending';
	$route['Payment/error'] 		= 'Midtrans/error';
	$route['Payment/finish'] 		= 'Midtrans/finish';
	$route['Payment/unfinish'] 		= 'Midtrans/unfinish';
	$route['Payment/notification'] 	= 'Midtrans/notification';
// Payment 