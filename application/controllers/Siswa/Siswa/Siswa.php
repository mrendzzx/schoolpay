<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Siswa extends Core_Controller{
    public function __construct(){
        parent::__construct();
        $this->auth_check();
        $this->siswaOnly();
        $this->session->set_userdata('title', 'Data Siswa');
    }

    public function index(){
       $this->load_content_admin('admin/siswa/index');
    }

    public function getTable(){
    	$where = '';
    	if($_GET['kelas'] != ''){
    		$kelaskunya = @explode('.', $_GET['kelas']);
            $where = " WHERE data_siswa.status_kelas = '".$kelaskunya[0]."' AND
                             data_siswa.program_studi = '".$kelaskunya[1]."' AND 
                             data_siswa.kode_kelas = '".$kelaskunya[2]."' ";
    	}
    	$query = "SELECT 
    				data_siswa.*, 
    				kelas.kelas 
    			FROM 
    				data_siswa 
    			INNER JOIN 
    				kelas 
    			ON 
    				data_siswa.status_kelas = kelas.id_kelas "
    				. $where .
    			" ORDER BY 
    				data_siswa.status_kelas, 
    				data_siswa.program_studi, 
    				data_siswa.kode_kelas, 
    				data_siswa.nama
    	";
        $res = $this->M_wsbangun->getData_by_query('default', $query);
        if ($res) {
            $callback = $res;
        }
        else{
        	$callback = null;
        }
        echo json_encode($callback);
    }

    public function add(){
    	$kelas = $this->M_wsbangun->getData('default','kelas');
    	$program_studi = $this->M_wsbangun->getData('default','program_studi');
    	$rombel = $this->M_wsbangun->getData('default','rombel');

    	$content = array(
    		'kelas' => $kelas,
    		'program_studi' => $program_studi,
    		'rombel' => $rombel
    	);

        $this->load->view('admin/siswa/add', $content);
    }

    public function getByID($id=""){
	    $where=array('nis'=>$id);
	    $res = $this->M_wsbangun->getData_by_criteria('default', 'data_siswa',$where);
	    // var_dump($res);die;
	    if ($res) {
            $callback = $res;
        }
        else{
        	$callback = null;
        }
        echo json_encode($callback);
	}

	public function save(){
    	$callback = array(
            'Data' => null,
            'Message' => null,
            'Error' => false
        );

        if($_POST){
        	$nis		= $this->input->post('nis', true);
            $nama		= $this->input->post('nama', true);
            $kelas		= $this->input->post('kelas', true);
            $prodi		= $this->input->post('prodi', true);
            $rombel		= $this->input->post('rombel', true);
            $status		= $this->input->post('status', true);
            $tipe		= $this->input->post('tipe', true);

        	$cekdata = $this->M_wsbangun->getData_by_query('default', "SELECT * FROM data_siswa WHERE nis = '$nis'");
    		if ($tipe == 'add') {
	        	if (count($cekdata) == 0) {
	        		$data = array(
	        			'nis'			=> $nis,
						'nama'			=> $nama,
						'status_kelas'	=> $kelas,
						'program_studi'	=> $prodi,
						'kode_kelas'	=> $rombel,
						'aktif'			=> $status,
						'lulus' 		=> 'T'
	        		);
			        $insert = $this->M_wsbangun->insertData('default', 'data_siswa', $data);
			        if ($insert) {
			        	$callback['Message'] = 'Data berhasil disimpan';
		                $callback['Error'] = false;
			        }
			        else{
			        	$callback['Message'] = 'Data gagal disimpan <br> ' . $insert;
		                $callback['Error'] = true;
			        }
	        	}
	        	else{
	        		$callback['Message'] = 'NIS sudah ada';
	                $callback['Error'] = true;
	        	}
    		}
    		elseif ($tipe == 'edit') {
        		$data = array(
        			'nis'			=> $nis,
					'nama'			=> $nama,
					'status_kelas'	=> $kelas,
					'program_studi'	=> $prodi,
					'kode_kelas'	=> $rombel,
					'aktif'			=> $status,
					'lulus' 		=> 'T'
        		);
	        	$where = array('nis' => $nis);

        		$update = $this->M_wsbangun->updateData('default', 'data_siswa', $data, $where);
		        if ($update) {
		        	$callback['Message'] = 'Data berhasil disimpan';
	                $callback['Error'] = false;
		        }
		        else{
		        	$callback['Message'] = 'Data gagal disimpan <br> ' . $update;
	                $callback['Error'] = true;
		        }
    		}
        }

        echo json_encode($callback);
    }

    public function activate(){
    	$callback = array(
            'Data' => null,
            'Message' => null,
            'Error' => false
        );

    	if ($_POST) {
    		$where = array('nis' => $this->input->post('id',true));
    		$cekdata = $this->M_wsbangun->getData_by_criteria('default', 'data_siswa', $where);

    		$data = array('aktif'=>"T");
    		switch ($cekdata[0]->aktif) {
    		 	case 'Y':
    		 		$data['aktif'] = 'T';
    		 		break;
    		 	case 'T':
    		 		$data['aktif'] = 'Y';
    		 		break;
    		 }
        	
    		$update = $this->M_wsbangun->updateData('default', 'data_siswa', $data, $where);
    		if ($update) {
	        	$callback['Message'] = 'Data berhasil disimpan';
                $callback['Error'] = false;
	        }
	        else{
	        	$callback['Message'] = 'Data gagal disimpan <br> ' . $update;
                $callback['Error'] = true;
	        }
    	}

    	echo json_encode($callback);
    }

    public function delete(){
        $callback = array(
            'Data' => null,
            'Message' => null,
            'Error' => false
        );

        if ($_POST) {
            $id = $this->input->post("id",true);
            $where = array('nis'=>$id);

            $del = $this->M_wsbangun->deleteData('default', 'data_siswa', $where);
            if ($del) {
                $callback['Message'] = "Data has been deleted successfully";
            }
            else {
                $callback['Message'] = $del;
                $callback['Error'] = true;
            }
        }
        echo json_encode($callback);
    }
}
