<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Dashboard extends Core_Controller{
    public function __construct(){
        parent::__construct();
        $this->auth_check();
        $this->siswaOnly();
        $this->session->set_userdata('title', 'Dashboard Siswa');
    }

    public function index(){
       $this->load_content_admin('siswa/index');
    }
}
