<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Pesan extends Core_Controller{
    public function __construct(){
        parent::__construct();
        $this->auth_check();
        $this->session->set_userdata('title', 'Pesan');
    }

    public function index(){
       $this->load_content_admin('admin/message/index');
    }
}
