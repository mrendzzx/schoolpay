<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Kartu extends Core_Controller{
    public function __construct(){
        parent::__construct();
        $this->auth_check();
        $this->session->set_userdata('title', 'Cetak Kartu');
    }

    public function index(){
       $this->load_content_admin('admin/kartu/index');
    }
}
