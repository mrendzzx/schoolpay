<!-- CONTENT -->
    <div class="app-content content">
        <div class="content-wrapper">
            <div class="content-wrapper-before"></div>
            <div class="content-header row"></div>
            <div class="content-body">
                <div class="row match-height">
                    <div class="col-md-12 col-lg-12 col-xl-12">
                        <h5 class="card-title text-bold-700 my-2">Aktivitas Pembayaran</h5>
                        <div class="card">            
                            <div class="card-content">
                                <div id="recent-projects" class="media-list position-relative">
                                    <div class="table-responsive">
                                        <table class="table table-padded table-xl mb-0" id="recent-project-table">
                                            <thead>
                                                <tr>
                                                    <th class="border-top-0">#</th>
                                                    <th class="border-top-0">Nama</th>
                                                    <th class="border-top-0">Status</th>
                                                    <th class="border-top-0">Payment</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td class="text-truncate align-middle">
                                                        <a href="#">1</a>
                                                    </td>
                                                    <td class="text-truncate">
                                                        rendy
                                                    </td>
                                                    <td class="text-truncate pb-0">
                                                        <span>Lunas</span>
                                                        <p class="font-small-2 text-muted">15th July, 2018</p>
                                                    </td>
                                                    <td>
                                                        <div class="btn bg-gradient-x-success">BCA VA</div>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<!-- CONTENT -->